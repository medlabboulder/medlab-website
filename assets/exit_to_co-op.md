% Exit to Co-op
% Nathan Schneider + Jason Wiener
% (Use lateral arrow keys to navigate)

## Can cooperative ownership improve your deal-flow?

We think it might.

## Who are we, anyway?

---

<img src="https://i2.wp.com/www.jrwiener.com/wp-content/uploads/2014/03/NoTie.jpg?fit=432%2C287" height="250px" />

### Jason Wiener


Principal at [Jason Wiener|P.C.](http://www.jrwiener.com/), "a boutique law and business consulting practice providing expertise to social enterprises and mission-driven business models"

---

<img src="https://nathanschneider.info/images/headshots/Schneider-Flatirons0-thumb.jpg" height="250px" />

### Nathan Schneider


[Assistant Professor of Media Studies](http://www.colorado.edu/cmci/people/media-studies/nathan-schneider) at the University of Colorado Boulder, leader in the ["platform cooperativism" movement](https://ioo.coop/); co-organizer of [the original conference](http://platform.coop/2015) and co-editor of the collective manifesto, _[Ours to Hack and to Own](http://www.orbooks.com/catalog/ours-to-hack-and-to-own/)_

---

## What is Platform cooperativism?

A growing movement to build democracy into the operating system of the internet, from worker-led startups to #BuyTwitter

---

## So, what's the problem?

---

## The internet economy needs better options

* Dominant platforms built on surveillance and monopoly
* Investor expectations have decentralized the network
* Problems start with early funding terms
* With fake news, hate speech, and bullying, investor interests conflict with those of users

---

### Investors need better options, too

How many companies in a given VC or angel portfolio either: 

* don't need subsequent financing
* are operating profitably
* are not likely to IPO or get acquired
* have a founder who doesn't want to sell

Is there latent value in your portfolio that a strategic co-op conversion can unlock?

Can we do better than a 1/10 success rate for liquidity?

---

### What's the addressable market we're talking about?

* % of convertible note portfolio

* \# of growing portfolio companies with no strategic buyer

* \# of mature portfolio companies ready to exit

---

## Where do co-ops come in?

*A new market of potential buyers*

* Users, workers, or other stakeholders buy the company from early investors

* The company benefits from the trust and loyalty that come through co-ownership and democratic governance

* Early investors benefit with a fair return

* All can celebrate the social benefit

---

## How would this actually work?

1. Identify candidates in fund portfolios for co-op conversions

2. Co-op capital partners finance buyout with exit to future co-owners

3. New co-owners pay off bridge capital with revenue from their business

---

## What might the terms look like?



---
### Example 1: Direct buy-out

Consider a portfolio company, **PortCo**, with steady cash-flow and a vibrant user community. The founder doesn't want to raise additional capital in a qualified financing; there is little to no likelihood of a convertible note converting. The fund prefers to extend the note rather than claim the principal with accrued interest.

<!-- * Independent and/or negotiated enterprise valuation -->
<!-- * User/worker/contributor capitalized buy-out through equity purchase and subscription payments -->

---

#### What does this mean for PortCo?

*  Founder negotiates enterpise valuation
*  PortCo undergoes user/worker/contributor buy-out of controlling interest
*  NewCoop becomes a multi-stakeholder cooperative with:
    * Worker owners (employees)
    * User owners (customers)
    * Contributor owners (contractors)
* Net profit allocated on basis of patronage and target dividend (or capped revenue share) to outside investors
* Fund converts at buy-out valuation or valuation cap, then holds preferred stock

---

###  Example 2: Leveraged buy-out

Consider a portfolio company, **TechCo**, that is growing rapidly and not yet profitable, but the founder doesn't want to exit. A mission-aligned private-equity buyer with co-op experience negotiates a buy-out with committed exit to user-ownership. There is little to no likelihood of a convertible note converting. The fund prefers to extend the note rather than claim the principal with accrued interest.

---

#### What does this mean for TechCo?

* Negotiated buy-out valuation to mission-driven private equity (PE)
* Fund gets converted at buy-out valuation or val. cap, then position liquidated
* PE takes control, restructures with balance of profit and growth
* Plans multi-year transition to user-ownership
* PE recoups investment through profitability
* Resulting entity is either earn-in or buy-in coop or trust

---

### Example 3: Seed stage

* Un-priced, quasi-convertible note investment
* Equity line of credit - 10% equity- preferred stock
* Repayment:
    * Discretionary cash distributions
    * Profit Sharing
    * 3x cap
    * If 3x cap is reached in < 3 years, 5% equity
* No need for exit or additional financing

(Adapted from [Indie.vc](http://indie.vc))


---

## What makes us think this really can work?

*or, why is this not utopian?*

---

### Namaste Solar

<img src="http://www.namastesolar.com/wp-content/uploads/2016/07/logo-footer-white-2.png" width="250px" />

A North Boulder solar pv company converted from a partnership to a worker-owned cooperative. Jason was in house counsel during the process.

---

### Stocksy United

<img src="https://tctechcrunch2011.files.wordpress.com/2013/03/screen-shot-2013-03-27-at-1-01-42-am.png?w=400" width="250px" />

A thriving, women-led stock-photo platform co-owned by hundreds of photographers, competing effectively in a competitive online market.

---

### dojo4

<img src="http://d33wubrfki0l68.cloudfront.net/images/global/f7a48faf00783246637567fce84518349358153c/a-2.png" width="250px" />

Successful Boulder technology services firm and social innovator converted from a partnership to a worker cooperative in early 2017, preserving flexibility and boosting a high-integrity brand.

---

### Associated Press

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Associated_Press_logo_2012.svg/416px-Associated_Press_logo_2012.svg.png" width="250px" />

Founded as a cooperative in 1846, it's no accident that nobody accuses this company of "fake news." Today it models the potential for co-ops in large-scale media.

---

### Twitter (?!)

<img src="https://upload.wikimedia.org/wikipedia/en/thumb/9/9f/Twitter_bird_logo_2012.svg/1267px-Twitter_bird_logo_2012.svg.png" width="250px" />

At the 2017 annual meeting, Twitter shareholders voted on a widely publicized proposal to study the possibility of co-op conversion. Is this as crazy as it sounds?

---

## How can we graft these models into the mainstream tech economy?

We want to build pathways for entrepreneurs and investors to embrace cooperative
models—and a fairer internet for everyone.


---

## We'd love your ideas and your help

* What players need to be involved to make this work? Incubators, accelerators, fund managers, private equity, legacy co-ops?
* What barriers exist to deploying capital?
* How do we identify and vet deals?
* Who would be receptive to continuing this conversation?
* What role might you play?

---

## How can you learn more?

[The Internet of Ownership](https://ioo.coop)   
Directory of the platform co-op ecosystem

_[Ours to Hack and to Own](http://www.orbooks.com/catalog/ours-to-hack-and-to-own/)_   
A collective manifesto on platform cooperativism


<br />

<table>
<tr>
<td width="50%">
**Nathan Schneider**
nathan.schneider@colorado.edu
</td>
<td width="50%">
**Jason Wiener**   
jason@jrwiener.com
</td>
</tr>
</table>
