---
layout: page
title: 'Radio'
permalink: /radio-podcast-looks-like-new/
---

<img src="{{ site.baseurl }}{% link assets/LooksLikeNew.png %}" style="width:40%; float:right; padding:0 0 10px 10px;" alt="Looks Like New" />

MEDLab's radio show and podcast, _Looks Like New_, asks old questions about new tech. Each month, we speak with someone who works with technology in ways that challenge conventional narratives and dominant power structures. The name comes from the phrase "a philosophy so old that it looks like new," repeated throughout the works of Peter Maurin, the French agrarian poet and co-founder of the Catholic Worker movement.

Listen to _Looks Like New_ the fourth Thursday of every month on [KGNU radio](https://www.kgnu.org/) at 6 p.m., or online as a podcast.

* [Archive](http://news.kgnu.org/category/looks-like-new/)
* [Stitcher](https://www.stitcher.com/podcast/kgnu-community-radio/looks-like-new-on-kgnu)
* [iTunes](https://itunes.apple.com/us/podcast/looks-like-new-on-kgnu/id1451526347)
* [RSS](http://news.kgnu.org/category/looks-like-new/feed/)
