---
layout: page
title: Events
permalink: /events/
---

{% if site.tags contains "events" %}
{% for post in site.tags["events"] %}
<p>
<a href="{{ post.url | relative_url }}">{{ post.title | escape }}</a> / <span class="post-meta">{{ post.event-date }}</span> - {{ post.summary }}
</p>
{% endfor %}
{% endif %}
