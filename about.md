---
layout: page
title: About
permalink: /about/
---

What does it take to build thriving media organizations, accountable to the communities they serve? At a time of global platform monopolies and local newspapers struggling to survive, the future of media will depend on the kinds of business models we design today.

The Media Enterprise Design Lab is a think tank for community ownership and governance in media organizations. It creates space for researchers and practitioners to challenge the conventional norms and explore possibilities offered by neglected histories and possible futures. Drawing on diverse fields such as cultural studies, law, management, media archaeology, organizational communication, and sociology, MEDLab holds space and time for better kinds of business.

## Programs

* **Collaboration** with startups and established organizations alike to imagine and develop transformative business models, ownership structures, and governance practices
* **Research** on under-explored strategies for organizational design, with a particular focus on models that support community wealth-building and appropriate accountability
* **Education** on media enterprise design through student fellowships and collaborations, together with public events and publications

MEDLab offers consultation services and produces public events. For more information on these and more, contact us at medlab@colorado.edu.

## Personnel

### Director

<img alt="Nathan Schneider" src="{{ site.baseurl }}{% link /assets/nathan_schneider.png %}" style="float:left; width: 200px; padding: 0 10px 10px 0px;" /> **[Nathan Schneider](https://www.colorado.edu/cmci/people/media-studies/nathan-schneider)** (assistant professor, Media Studies) works at the intersections of technology and social change, particularly in efforts to develop more democratic business models for the online economy. He is the author of three books, most recently _[Everything for Everyone: The Radical Tradition That Is Shaping the Next Economy](https://nathanschneider.info/e4e)_, and he co-edited _[Ours to Hack and to Own: The Rise of Platform Cooperativism, a New Vision for the Future of Work and a Fairer Internet](https://nathanschneider.info/books/books/ours-to-hack/)_. His articles have appeared in publications such as _Harper's_, _The Nation_, _The Guardian_, _Vice_ and _The Catholic Worker_. Find more information at [nathanschneider.info](https://nathanschneider.info/).

### Research fellows

<img alt="Boushra Batlouni" src="{{ site.baseurl }}{% link /assets/boushra_batlouni.jpg %}" style="float:left; width: 200px; padding: 0 10px 10px 0px;" />**[Boushra Batlouni](https://www.colorado.edu/cmci/people/graduate-students/media-studies/boushra-batlouni)** (PhD student, Media Studies) wants to investigate the intersections between global startup culture and mediated narratives of innovation and success. She is interested in the structures and rituals that emergent technologies impose upon us, and how they shape our capacities for imagining different futures. She is also fascinated by the role that basic, and particularly base, human emotions can play in constructing and designing potentials to instigate positive change.

<div style="clear:both"></div>

<img alt="Laura Daley" src="{{ site.baseurl }}{% link /assets/laura_daley.jpg %}" style="float:left; width: 200px; padding: 0 10px 10px 0px;" />**[Laura Daley](https://www.colorado.edu/cmci/people/graduate-students/media-studies/laura-daley)** (MA student, Media and Public Engagement) is a digital arts scholar with a strong interest in the politicization of health, particularly for women and marginalized groups, and its relationship with media. With a passion for creative video production and an eye toward social justice, she hopes to create impact-driven media that challenge traditional power structures. She holds a Bachelor's degree in Writing, Rhetoric, & Communication and Spanish Language & Literature from Transylvania University in Lexington, KY.

<div style="clear:both"></div>

<img alt="Cassandra Dana" src="{{ site.baseurl }}{% link /assets/cassandra_dana.jpg %}" style="float:left; width: 200px; padding: 0 10px 10px 0px;" />**[Cassandra Dana](https://www.colorado.edu/cmci/people/graduate-students/media-studies/cassandra-dana)** (MA student, Media and Public Engagement) is passionate about creating exhibition opportunities for media that features traditionally underrepresented populations. Prior to beginning her degree Cassandra worked as the Production and Marketing Manager for the Martha’s Vineyard Film Festival, and as a Marketing and Public Relations Coordinator in Providence, Rhode Island. Cassandra’s interests include queer theory, gender representation and media’s role in norm evolution. Cassandra holds a BA in sociology and film studies from the University of Colorado. 

<div style="clear:both"></div>

<img alt="Mark Fairbrother" src="{{ site.baseurl }}{% link /assets/mark_fairbrother.jpg %}" style="float:left; width: 200px; padding: 0 10px 10px 0px;" />**[Mark Fairbrother](https://www.colorado.edu/cmci/people/graduate-students/media-studies/mark-fairbrother)** (MA student, Media and Public Engagement) aims to build media productions and products that challenge injustice, promote civil dialogue, and inspire collective action. His interests include entertainment media, dynamic education, and sustainable development throughout the Americas. Prior to his graduate work, he enjoyed lengthy stints in Latin America (y por eso sí habla español). He holds a bachelor’s degree in public relations and leadership from Central Michigan University.

<div style="clear:both"></div>

<img alt="Brady McDonough" src="{{ site.baseurl }}{% link /assets/brady_mcdonough.jpg %}" style="float:left; width: 200px; padding: 0 10px 10px 0px;" />**[Brady McDonough](https://www.colorado.edu/cmci/people/graduate-students/media-studies/brady_mcdonough)** (MA student, Media and Public Engagement) is interested in media representation of the Queer community in both niche and mainstream markets combined with the development of Queer culture and economy on a societal scale. His undergraduate work focused on subcultural appropriation of normative characters and symbols and performative sexuality. Aside from a service-based career, Brady also worked in radio, finance, and gender equity during his undergraduate experience. Brady holds a BA in Communications – Media Studies, Global Studies from the University of Wisconsin – Superior.

<div style="clear:both"></div>

<img alt="R. M. Shea" src="{{ site.baseurl }}{% link /assets/ryan_shea.png %}" style="float:left; width: 200px; padding: 0 10px 10px 0px;" />**[Ryan Shea](https://shea.cool/)** (undergraduate student, Computer Science) is interested in startups and new technologies. He has interned at local blockchain companies in Colorado and has organized events in the Boulder entrepreneurial community. When he's not behind a screen, you can find him on the ski slopes or looking into a camera.

<div style="clear:both"></div>

### Faculty fellows

* **[Lori Emerson](https://www.colorado.edu/cmci/people/iawp/lori-emerson)** (associate professor, English and Intermedia Arts, Writing, and Performance; founding director of the [Media Archaeology Lab](https://mediaarchaeologylab.com/))
* **[Casey Fiesler](https://www.colorado.edu/cmci/people/information-science/casey-fiesler)** (assistant professor, Information Science)
* **[Jolene Fisher](https://www.colorado.edu/cmci/people/advertising-public-relations-and-media-design/jolene-fisher)** (assistant professor, Advertising, Public Relations and Media Design)
* **[Steven Frost](https://www.colorado.edu/cmci/people/media-studies/steven-frost)** (instructor, Media Studies; host of [Colorado Sewing Rebellion](https://www.stevenfrost.com/portfolio/colorado-sewing-rebellion/))
* **[Tim Kuhn](https://www.colorado.edu/cmci/people/communication/timothy-kuhn)** (associate professor, Communication)
* **[Christine Larson](https://www.colorado.edu/cmci/people/journalism/christine-larson)** (assistant professor, Journalism)
* **[Mark Meaney](https://www.colorado.edu/business/mark-meaney)** - (executive director, [Center for Ethics and Social Responsibility](https://www.colorado.edu/business/CESR/))

### Community fellows

* **[Danny Spitzberg](https://twitter.com/daspitzberg/)** (user researcher for a cooperative economy)
* **[Cadwell Turnbull](https://cadwellturnbull.com/)** (speculative writer and grassroots activist, author of _The Lesson_)
* **[Jason Wiener](https://jrwiener.com/team/jason/)** (president, Jason Wiener\|p.c.)
* **[Mara Zepeda](https://www.marazepeda.com/)** (founder, Switchboard and Zebras Unite)

### Alumni

Katy Fetters (founder, [#CPSTRONG](https://cerebralpalsystrong.com/)) <!--use commas to separate subsequent entries-->
