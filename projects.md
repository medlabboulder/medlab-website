---
layout: page
title: Projects
permalink: /projects/
---

{% for project in site.projects %}
### <a href="{{ project.url | relative_url }}">{{ project.title }}</a>

<p style="padding-bottom:1.5em;">{{ project.summary }}</p>

{% endfor %}
