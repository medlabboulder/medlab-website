---
layout: project
title: Internet of Ownership
slug: internet-of-ownership
summary: Supporting an ecosystem for democratic business in the online economy
---

Since [reporting on](https://www.shareable.net/blog/owning-is-the-new-sharing) an emerging interest in ownership among tech entrepreneurs in 2014 and co-organizing the first "[platform cooperativism](https://platform.coop/)" conference with Trebor Scholz at the New School in 2015, MEDLab director Nathan Schneider has been active in organizing and documenting this growing movement aiming to build an ecosystem for cooperative business in the online economy.

MEDLab has focused on developing the concept of "exit to community" (E2C), a set of narratives and strategies to re-orient the startup economy toward community ownership and accountability.

*Supported by fellowships from the Open Society Foundations and Rutgers University's [Institute for the Study of Employee Ownership and Profit Sharing](https://smlr.rutgers.edu/content/institute-study-employee-ownership-and-profit-sharing).*

## Publications

* [The Internet of Ownership](https://ioo.coop/), a resource-sharing network that includes a directory of the platform co-op ecosystem
* Morshed Mannan and Nathan Schneider, "[Conversion to Accountability: Strategies for Multi-Stakeholder Ownership in the Platform Economy](https://nathanschneider.info/conversion-strategies)"
* Nathan Schneider and Jason Wiener, "[Exit to Co-op](/medlab/assets/exit_to_co-op.html)," concept slide deck (2017)
* Nathan Schneider, "[VirtualCoop](/medlab/assets/VirtualCoop.pdf)," concept whitepaper (2018)
* Nathan Schneider, "[An Internet of Ownership: Democratic Design for the Online Economy](https://journals.sagepub.com/doi/abs/10.1177/0038026118758533)," _The Sociological Review_ 66, no. 2 (March 2018)
* Nathan Schneider, "[Startups Need a New Option: Exit to Community](https://hackernoon.com/startups-need-a-new-option-exit-to-community-ig12v2z73)
* Trebor Scholz and Nathan Schneider (eds.), _Ours to Hack and to Own: The Rise of Platform Cooperativism, a New Vision for the Future of Work and a Fairer Internet_ (OR Books, 2016)

## Collaborators

* [Curafied](https://curafied.com/)
* [Sarapis](https://sarapis.org/)
* [Zebras Unite](https://zebrasunite.com)
