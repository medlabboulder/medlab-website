---
layout: project
title: Ethical EdTech
slug: ethical-edtech
summary: Connecting educators with technology that doesn't spy or exploit
---

Education represents a valuable market for powerful technology companies, and the tools they offer often collect student data and normalize students' use of their commercial services. This project seeks to collect, package, and make more readily available the wide variety of privacy-oriented, hackable, free/libre/open alternatives that frequently lack the marketing resources to compete for educators' attention.

## Publications

* [EthicalEdTech.info](https://ethicaledtech.info/), the project's collaborative wiki

## Collaborators

* [Erin Rose Glass](https://library.ucsd.edu/about/contact-us/librarians-and-subject-specialists/erin-glass.html), University of California, San Diego
