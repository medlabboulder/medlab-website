---
layout: project
title: Stakeholder Ownership for Sustainable News
summary: Can democratic ownership design foster newfound trust and revenue for journalism?
---

In collaboration with pioneering publications, we are exploring how democratic strategies for ownership, financing, and governance can support a new generation of accountable, sustainable news organizations.

*Supported by grants from the Brett Family Foundation and the CU Boulder Office for Outreach and Engagement.*

## Collaborators

* _[The Colorado Sun](https://coloradosun.com/)_

## Publications

* "[_Colorado Sun_ Benefit Corporation Evaluation](/medlab/assets/SunEval-2019.pdf)," Media Enterprise Design Lab (August 2019)
* Nathan Schneider, "[Broad-Based Stakeholder Ownership in Journalism: A Legacy and a Strategy](https://osf.io/g4e6t/?view_only=e9cb90e1f67c4aaab97666b213c75d75)" (working draft)

## Related

* Open letter from CU Boulder faculty: "[CMCI Faculty Agree: Local News Matters](https://www.colorado.edu/cmci/localnewsmatters)"

