---
layout: project
title: Collaborative Governance
slug: collab-gov
summary: Exploring leading-edge democratic governance and leadership, especially through digital tools
---

We are conducting consulting, convening and research on leading-edge democratic governance and leadership, especially through digital tools.

## Publications

* [CommunityRule](https://medlabboulder.gitlab.io/communityrule/), "a governance toolkit for great communities"
* [Democratic Mediums](https://medlabboulder.gitlab.io/democraticmediums/), "a directory of patterns for decision, deliberation, and noise"
* Nathan Schneider, "[Admins, Mods, and Benevolent Dictators for Life: The Implicit Feudalism of Online Communities](https://nathanschneider.info/implicit-feudalism)"

## Collaborators

* [Action Network](https://actionnetwork.org/)
* [Metagovernance Project](http://metagov.org/)
