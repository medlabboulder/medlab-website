---
layout: project
title: Shared Ownership in Colorado
slug: shared-ownership-in-colorado
summary: Bringing together cooperatives, credit unions, and employee owned companies across the state
---

In 2018, together with the Center for Ethics and Social Responsibility at CU Boulder's Leeds School of Business, we hosted a conference called the [Colorado Shared Ownership Summit](https://calendar.colorado.edu/event/colorado_shared_ownership_summit), which brought together cooperatives, credit unions, and employee owned companies from across the state. In its wake, we are working to develop the policy and support infrastructure to help Colorado become even more a leader in democratic business.

*Supported by Cooperatives for a Better World, CoBank, the Colorado Rural Electric Association, the Rocky Mountain Farmers Union, and the CU Boulder Office for Outreach and Engagement.*

## Collaborators

* [Colorado.coop](https://colorado.coop)
* [Colorado Co-ops Study Circle](https://coloradocoops.info/) 
    - _[Co-op Power Hour](https://coloradocoops.info/radio/)_ radio show and podcast on KGNU
    - [Directory](https://coloradocoops.info/directory) of Colorado co-ops

## Press

* Jackson Barnett, "[Colorado co-ops seeing a revival in state with strong ties to 'radical tradition,'](https://www.denverpost.com/2019/03/15/cooperatives-revival-colorado/)" _Denver Post_ (March 15, 2019)
* Pratik Joshi, "[Co-op conference at CU focuses on capitalism with conscience](http://www.dailycamera.com/top-business/ci_32257849/co-op-conference-at-cu-focuses-capitalism-conscience)," _Daily Camera_ (November 7, 2018)

## Publications

* "[A Cooperative Colorado: Findings from the Cooperative Policy Roundtable](https://coloradocoops.info/2019/05/02/a-cooperative-colorado-findings-from-the-cooperative-policy-roundtable/)," Colorado Co-ops Study Circle (May 1, 2019)
* "[Understanding Shared Ownership: This Year’s Conscious Capitalism Conference Theme](https://www.colorado.edu/business/2018/06/26/understanding-shared-ownership-years-conscious-capitalism-conference-theme)," CESR blog (June 26, 2018)

## Related

* Aldo Svaldi, "[Colorado launches initiative to boost employee ownership of businesses](https://www.denverpost.com/2019/03/22/colorado-employee-owned-business-initiative/amp/)," _Denver Post_ (March 22, 2019)
* Jason Wiener and Linda Phillips, "[Colorado---'The Delaware of Cooperative Law'](https://medium.com/fifty-by-fifty/colorado-the-delaware-of-cooperative-law-babedc9e88eb)," Fifty by Fifty blog (May 29, 2018)
