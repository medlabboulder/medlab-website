#!/bin/bash
echo "Deleting at target"
lftp -p 21 -u $CU_USR,$CU_PASSWORD $CU_HOSTNAME -e "set ftp:ssl-allow no; cd $CU_DIR; glob -a rm -r *; bye"

echo "Uploading"
cd _site || exit
lftp -p 21 -u $CU_USR,$CU_PASSWORD $CU_HOSTNAME -e "set ftp:ssl-allow no; mirror -R . $CU_DIR; bye"
