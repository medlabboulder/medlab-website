---
layout: post
title: "Meet KOSAKTI, a Creative New Cooperative in Indonesia"
summary: "How one organization is enabling many more to get started"
author: Nathan Schneider
tags: [collab-gov, internet-of-ownership]
---

*The following is a lightly edited email exchange I had with Bimo Ario Suryandaru, CEO of KOSAKTI, a cooperative in Indonesia. He and his team are developing an interesting approach that I was grateful to learn about, and I thought others might be, too. [See their slide deck here](http://kosakti.id/wp-content/uploads/2019/02/KOSAKTI-PROFILE-compressed.pdf).*

**What does KOSAKTI exist to do?**

Our biggest goal is quite simple: to erase inequality in all of its aspects. By that, we don't mean that everything should be equal, we only want to make it just, economically speaking. KOSAKTI was established by former NGO members who came to a realization that they could not move forward just driven by donors. Community development should be done in a reciprocal way. Therefore they established a cooperative business.

**That is a very ambitious goal. How did you find us at MEDLab?**

I was reading [your VirtualCoop concept proposal](https://cmci.colorado.edu/medlab/assets/VirtualCoop.pdf) after my friend showed it to me. Apparently, we are doing a similar model that you propose with our cooperative, KOSAKTI, in Indonesia. It is not an easy road, considering the cooperative model is not a very popular choice here. In addition, our model is very unique and new.

**The idea of VirtualCoop is a kind of umbrella organization that includes within its legal structure startup co-ops within it. Is that what you do?**

After KOSAKTI failed to run its own businesses twice, I suggested, why don't we just support people (especially our members) who are already running their business well yet still need some help? In any way possible: business consultation, management, innovation, legal, and much more.

Originally, we thought this model already existed. But when Camilla Carabini from Coopermondo came to Indonesia, we used thrd opportunity to ask her about this model. She said it had never crossed her mind—other than the ways it resembles a federation model or a confederation.

**What kinds of projects are part of the co-op? What kinds of businesses are they in?**

I don't think one email is enough to share everything we're doing here. Generally, we're involved in consultation and assistance on cooperative business development. A few examples: we're doing a co-owned direct-trade promotion integrated business in coffee, coconut sugar, and other agriculture commodities; a film-industry cooperative; community cricket farming; community daycare and so on.

**Are any of the companies using KOSAKTI's legal incorporation, rather than incorporating independently themselves? Or all of them? How does the legal relationship work?**

Most of them, yes. We use KOSAKTI's legal incorporation as a cooperative. However, they still have an option if they someday wanted to incorporate independently. On the relationship matter, they only need to become our cooperative member to use any of our services, including legal incorporation. I'm not saying it is working smoothly; right now we are facing problems, such as business permit limitation for a single incorporation.

**Are any of the companies within the co-op themselves co-ops? You mention that you support conversions. What has that looked like?**

Yes, there is one of our partners that is a co-op and also others that already have business legal entities such as private company or foundation. We treat those as a business partner rather than a member.

**Do you operate primarily online, or through in-person interactions and relationships?**

For now, we are operating primarily through person-to-person interactions and relationships since we are still working in small scale. It is one of our main goals to be able to offer our service online, and we are working on it by developing our website and mobile apps. We hope to be able to collaborate with other similar cooperative or entities out there, that's why we have a special interest in platform cooperatives.

**Can you tell me a bit about the scale of your company?**

We are a small cooperative, with only just 140 members and ten business units. We have been running for only three years. We are working through many sectors with only just $1,500 in the organisation's bank account and $10,000 in losses last year. It is a very new concept and not very many people like it, but it is fun. 

**That's a tough spot to be in. What would success look like to you, in the long run?**

I have to ask you back the same question for this one… We don't really have a clear view, actually. So far, we are pretty optimistic that this model could make many things so much better and also so much easier for new co-ops. Making people notice this ownership issue as an important thing is more than enough success for us, and making it understandable is way more.
