---
layout: event
title: "Exit to Community: Legal Options"
summary: "Expect to come away with actionable tools for engineering your own E2C"
tags: [internet-of-ownership, events]
event-date: January 24, 2020
event-time: 10-11:00 a.m. Mountain Time
event-location: Webinar
---

<a href="{{ site.baseurl }}{% link /assets/e2c/000124_Webinar_web.png %}"><img alt="Graphic recording of the session by Sita Magnuson of Dpict." src="{{ site.baseurl }}{% link /assets/e2c/000124_Webinar_web.png %}" style="width:100%" /></a>

<em>Graphic recording of the session by Sita Magnuson of Dpict. <a href="{{ site.baseurl }}{% link /assets/e2c/000124_Webinar_print.jpg %}">Download the print version here</a>.</em>

A growing network of entrepreneurs, activists, and investors are exploring the possibility of "exit to community"—enabling startups to transition toward ownership by their core stakeholders. There are a variety of possible pathways toward this goal, as well as a variety of challenges that stand in the way. Both the pathways and challenges depend on the underlying law.

In this webinar, three pioneering legal experts will share the strategies they have been developing for enabling various forms of exit to community. We will leave most of the time for a facilitated discussion based on questions from webinar participants. Expect to come away with actionable tools for engineering your own E2C.

<iframe src="https://archive.org/embed/exit_to_community-legal_options" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

## Speakers

* Camille Kerr (Upside Down Consulting)
* Morshed Mannan (Leiden Law School)
* Jason Wiener (jason wiener \| p.c.)

## Resources

* “[Startups Need a New Option: Exit to Community](https://hackernoon.com/startups-need-a-new-option-exit-to-community-ig12v2z73),” *Hacker Noon* (September 16, 2019)
* "[Exit to Community: A New Option for Startups?](https://cmci.colorado.edu/medlab/2019/12/01/exit-to-community-webinar.html)," MEDLab webinar (December 11, 2019)
* “[Meetup to the People: How a Zebra could Rise from a Unicorn’s Fall](https://medium.com/@sexandstartups/meetup-to-the-people-how-a-zebra-could-rise-from-a-unicorns-fall-cfa93d83bcdc),” @sexandstartups on Medium (November 5, 2019)
* "[Conversion to Accountability: Strategies for Multi-Stakeholder Ownership in the Platform Economy,](https://docs.google.com/document/d/1SFUklZmxDHVKU-fXlXc8C5JDmPuAJU9CN7Oklgjghck/edit#heading=h.mih5fqhxywjl)" working draft

*Hosted by the [Media Enterprise Design Lab](http://cmci.colorado.edu/medlab/) at the University of Colorado Boulder and [Zebras Unite](https://www.zebrasunite.com/), with support from the Open Society Foundations, which are not responsible for the content.*

<img alt="Zebras Unite" src="{{ site.baseurl }}{% link /assets/zebras_unite_logo.jpg %}" style="width:100%" />


