---
layout: event
title: "Alt-Startup: Intro to Nonprofits and Community Ownership"
summary: "A workshop for New Venture Challenge participants"
tags: [events]
event-date: January 22, 2020
event-time: 3-5 p.m. Mountain Time
event-location: Center for Academic Success and Engagement (CASE), E422
---

How can you anchor your startup to a social mission? How can you make it accountable long-term to the communities it is meant to serve? This session, led by members of CMCI's Media Enterprise Design Lab, introduces several strategies for protecting social enterprise, including nonprofits, benefit corporations and cooperatives.

**[Register here](https://calendar.colorado.edu/event/alt-startup_intro_to_nonprofits_and_community_ownership).**


