---
layout: event
title: "If I Only Had a Heart: Accounting for Care Work in Organizations"
summary: Activist Stacco Troncoso introduces the Decentralized Cooperative Organization
tags: [internet-of-ownership, events]
event-date: November 18, 2019
event-time: 3-4:30 p.m. Mountain Time
event-location: CASE E422
---

<img alt="Poster by Cassandra Dana" src="{{ site.baseurl }}{% link /assets/TroncosoPoster.png %}" style="width:100%" />

**[Listen to the conversation here](https://news.kgnu.org/2019/11/looks-like-new-can-business-account-for-care-work/).**

Too often, the necessary care work that generates and sustains our lives occurs at the margins of the economy, unaccounted for and under-recognized. Spain-based activist [Stacco Troncoso](https://stacco.works/) is part of a movement to change that. Troncoso, a member of the Guerrilla Media Cooperative, is part of a collective developing a new model—the DisCO, or Distributed Cooperative Organization. It’s a set of organizational tools and practices for people who want to work together in a cooperative, commons-oriented, and feminist economic form. It’s also an alternative to the logic of blockchain-based Decentralized Autonomous Organizations, or DAOs.

**[Register here](https://cloud.medlab.host/apps/forms/form/9ewtcuacnmye2lrz)**, and [here is a map to the CASE building on CU Boulder's campus](https://www.colorado.edu/map/?id=336#!m/347439).

Read more about the DisCO concept at _[Hacker Noon](https://hackernoon.com/last-night-a-distributed-cooperative-organization-saved-my-life-a-brief-introduction-to-discos-4u5cv2zmn)_.
