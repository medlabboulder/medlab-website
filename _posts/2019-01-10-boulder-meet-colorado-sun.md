---
layout: event
author: Nathan Schneider
title: "Event: Boulder, Meet The Colorado Sun"
summary: Join editors and reporters for a celebration of what they have accomplished
tags: [stakeholder-news, events]
event-date: February 15, 2019
event-location: Boulder, CO
---

**[Register here](https://www.eventbrite.com/e/boulder-meet-the-colorado-sun-tickets-54663624372)**

Out of widespread consolidation and layoffs in Colorado journalism, a new publication emerged last summer, The Colorado Sun. After just a few months, it has already produced vital reporting from across the state. The Sun is also journalist-owned and affiliated with the cryptocurrency startup Civil.

Join Colorado Sun editors and reporters, together with CU Boulder students who have been collaborating with them, for a celebration of what they have accomplished. Learn about their work and their business model, and find out how you can get involved in a renaissance for news-gathering in our state.

*Hosted by the Media Enterprise Design Lab at CU Boulder's College of Media, Communication and Information, with support from the university's Office for Outreach and Engagement and the Brett Family Foundation.*
