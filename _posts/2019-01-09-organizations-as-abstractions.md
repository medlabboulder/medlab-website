---
layout: post
author: Nathan Schneider
title: Organizations as Abstractions Over the Law
summary: How democratic innovators are hacking incorporation statutes
tags: [internet-of-ownership, collab-gov]
---

The law, perhaps by definition, lags behind people working for social change. I certainly found this over and over in the next-generation cooperative projects I profiled in _[Everything for Everyone](https://nathanschneider.info/e4e)_. One co-op in Catalonia was, legally, a mishmash of entities that presented themselves as if they were a coherent whole; another, in New Zealand, was an LLC that called itself a foundation but operated like a co-op. MEDLab has been working with Action Network, whose founder [describes](https://civichall.org/civicist/build-tech-with-movements/) its innovative governance model as "cooperative," even though the organization is mainly a 501(c)(4) nonprofit. Some of these co-ops are more cooperative in practice than many "actual" co-ops; it's just that the older co-op law was inadequate to meet their needs. They had to hack.

Really, no organization is what the incorporation statutes and bylaws say it is. Organizations are made of people, culture, relationships, and other things that don't fit into the letter of our laws, and which shouldn't. This is especially the case for democratic enterprises trying to operate in a legal regime designed primarily for control by large investors and wealthy donors. Those seeking to develop new strategies for more accountable organizations have to be clever. They have to build the organizational structure as a [layer of abstraction](https://en.wikipedia.org/wiki/Abstraction_layer) quite distinct from the legal layer.

Here are some examples of strategies I am talking about:

* The Sustainable Economies Law Center supports and trains **[worker self-directed nonprofits](https://www.theselc.org/worker_selfdirected_nonprofits)** (and is one itself), which operate as worker co-ops within the context of a 501(c)(3) organization
* Some cooperatives **[form as LLCs or other business entities](https://www.shareable.net/blog/forming-a-worker-coop-llc-or-cooperative-corporation)** and encode their cooperative practices in bylaws and contracts, rather than at the level of incorporation
* A startup, [Staffing Cooperative](https://staffing.coop/), is developing a new model in which **the co-op serves as a holding company** for non-cooperative subsidiaries, whose workers, in turn, become members of the co-op
* [I have proposed a strategy]({{ site.baseurl }}{% link assets/VirtualCoop.pdf %}) based on nonprofit fiscal sponsorship, through which **early-stage co-ops can form without incorporation** by operating within an umbrella entity, which may or may not be itself a co-op

Hacking up abstractions, however, runs into limits. For one thing, there can be tremendous advantages in taxation and regulatory treatment from using an incorporation statute designed with your kind of organization in mind; for instance, co-ops can facilitate early-stage community investment in ways that are simply not feasible for other companies. In some jurisdictions, too, it is outright illegal to call anything a co-op that is not incorporated as such. Finally, there is the danger of slippage. Many co-ops that are generations old have lost much of their democratic culture, and the only thing retaining even a modicum of accountability to their members is the fact that they are stuck in a cooperative legal statute. If democracy isn't hard-coded into the organization, it becomes that much more vulnerable to defaulting back to the dominant, less-democratic paradigm.

Misaligned legal and organizational layers can exact a cost over time. [May First/People Link](https://mayfirst.org/en/), an activist-oriented tech provider, is a nonprofit that has long identified as a "democratic membership organization," in which member organizations and individuals elect the board. Again, it is probably more cooperative than many actual co-ops. But currently, the organization is discussing [a proposal](https://comment.mayfirst.org/t/mm-2018-form-a-committee-to-explore-converting-our-organization-to-a-multi-stake-holder-cooperative/910) to make the legal transition to cooperative status. Over the years, MFPL's democratic tendencies have generated both regulatory and organizational friction. Those advocating this shift see it as a means of "resolving contradictions and weaknesses in our current democratic structure"---and to more transparently communicate to members what their relationship and responsibilities to the organization should be.

Hacking the law may be a strategy for innovation, but in the long run it probably can't be a substitute for changing the law as well.

*Thanks to Brian Young of Action Network and Camille Kerr of Staffing Cooperative for their contributions to this conversation.*
