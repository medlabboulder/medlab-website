---
layout: post
author: Arielle Jordan
title: "The Curafied Co-op Story: A Prequel"
summary: '"That, ladies and gentlemen, is how you disrupt an industry"'
tags: [internet-of-ownership]
---

<img alt="Arielle Jordan" src="{{ site.baseurl }}{% link /assets/arielle_jordan.png %}" style="width: 100%;" />

I’m the founder and CEO of Curafied. Curafied is a platform that helps digital content creators earn monthly income by charging a low monthly subscription to fans for their content creation and curation directly—not from “patron” donations or advertising sponsorships. We’re exploring a new direction for our startup, and I would like to tell you about it in case you have any feedback or want to help.


The concept of creator compensation platforms is not new, however my aim has been to improve on the current models. While I don’t consider myself a content creator, I am certainly a (huge) content consumer. Call me a Millennial, but there is a MULTITUDE of things—from home projects to anxiety management—that I have successfully navigated with the sole help of blog posts, podcasts, and online videos. Such is the power of content creators in today’s world.


But also in today’s world, the “Big Social” platforms are profiting tremendously while the individuals who contribute their time and energy posting to these platforms are all too often left in the dust, seeing only pennies as compensation, if even that. As much as I would love to help these creators financially, “patronizing” by making donations seems a bit off-putting. I can’t help but think, “No one is making donations to me to help me pay my bills,” and, “The content is still going to be free anyway.” But the plight of content creators is important to me and I wanted to figure out a different way I could help—so I went ahead and built a platform that would allow content consumers like me to support creators directly for the ability to access some or all of their work. This seemed like a more reasonable tradeoff than the misleading concept of donations or a mere tip jar somewhere.


The financial piece is just one of the problems with Big Social. Creators and influencers also face losing exposure through algorithms that change on a whim and being at the mercy of a handful of often egregious policies that they have zero control over, including advertiser preferences and even loss of content ownership rights. These are issues I wanted to tackle with my platform as well.  


After a few months of mulling this over, I pitched the concept of Curafied at Dayton Startup Week in 2017.  A female angel investor was intrigued by the concept and business model and we began working together to take this idea to a prototype.   A few more angels hopped on board and the first iteration of Curafied launched just at the end of that same year.


The web-only platform we built was set up such that creators could sign up for free and start posting videos, photos, and blog posts behind a paywall. We call this content space a Collection. Fans and followers of the creator pay a small subscription to access his or her Collection, as well as access to a subscriber-only live chat. For every subscription to their Collection, a creator receives a portion of the sale.


As my team and I set out to “disrupt” the influencer space, we very quickly ran into a stream of issues that included: 


* not enough differentiation from other platforms attempting to solve the same problem
* a business model that wasn’t met with too much enthusiasm by creators
* too little funding to build a more robust and user-friendly platform for them
* an overall idea that wasn’t interesting enough to VCs because “content monetization isn’t sexy”


Months—which turned into over a year—went by and we weren’t making much progress with our platform buildout or user signups. As if we didn’t have enough obstacles in our way, I am an Ohio-based, mixed Black, female, first-time, single founder, which is pretty much a death sentence in the tech startup world. I experienced so many meetings where I was belittled, talked over, yelled at, and even yawned at by potential new investors, I was beginning to feel like we wouldn’t have a chance in hell at success. 


Pressure was starting to mount from our current angel investors, and although it was starting to seem like all was about to be lost, proverbial lightning flashed. “Have you ever considered a co-op model for Curafied?” my first angel investor asked me. When I told her I wasn’t sure how it could apply, she introduced me to a literal angel: Rachel Meketon at Co-op Dayton. Rachel and her organization help businesses determine if and how the cooperative model is a viable one for their mission and goals, and provide them with the resources to move forward with the conversion.


Before we met in person, Rachel and I exchanged a series of emails and had a few phone calls that reignited my imagination of a world with Curafied in it. First, she explained to me that co-ops aren’t just for industrial workers or grocery store patrons, which were the only co-ops I had ever heard of before. She then gave me some examples of different types of cooperatives and how they serve to benefit those who contribute as members by giving them direct ownership in the company, including voting rights and a share of its profits. 


MIND. BLOWN. This is exactly what was missing from Curafied’s current, VC-backable-unicorn-wannabe-fueled business model. In the midst of all the stress and setbacks that had mounted from the beginning of forming this company, I had lost sight of why I wanted to create Curafied in the first place: to give contributing content creators and curators more control over their financial affairs than current social and digital media platforms allow. Curafied as a platform co-op could actually give its contributors the most financially rewarding option that exists today. Through co-ownership, creators can be sure that the platform's cut is really a reinvestment in their own work—and that they'll see a cut, in return, when we're profitable. And that, ladies and gentlemen, is not only how you disrupt an industry, but also how you do so with your users absolute best interest in mind. It’s a win-win.


I immediately began researching and seeking out other platform co-ops. I’ve been learning from amazing people like Margaret Vincent, VP Legal at Stocksy United, a platform co-op for Stock Photographers, and Jason Weiner, a prominent cooperative platform attorney and strategist. 


My conversations with all of them, coupled with my preliminary research, made the idea of converting Curafied to a platform co-op for content creators seem quite promising. With my angel investors also becoming excited by this potential pivot, I began trying to piece together what our co-op would like. Who would be the members? And what would be the conditions of their membership? A few concerns popped into my mind, such as how the goal of a platform, generally, is to get as many people using it as possible; no user caps. As a co-op, would Curafied have to limit the number of content creators and curators we accept? And what about my current investors? Where would they fit in? What about future investors? Is that even a thing with platform co-ops?! How do investors view platform co-ops?


These are the types of questions I aim to answer this summer through user research and a personalized workshop with Jason Weiner and his team of cooperative business consultants. My goal is to have identified the specific type of content creator we want using the Curafied MVP (minimum viable product), what that MVP needs to be, and how to best structure and accommodate our different membership classes.


The task at hand is no small one. And I realize that I’ll continue to face many ups and downs on this journey. But I’m confident that the work we’re doing, at the very least, will challenge the way larger platforms treat their contributors. I hope (if this works) that Curafied can serve as one of many examples of how the cooperative model can achieve better experiences and better economies—for everyone involved.


**If you would like to reach me to learn more or to explore becoming part of this process, please contact arielle@curafied.com.**
