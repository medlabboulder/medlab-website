---
layout: post
title: "Become Better Together with Enspiral"
summary: "Lessons in community-based business from New Zealand"
author: Nathan Schneider
tags: [collab-gov]
---

![Better Work Together cover](https://static1.squarespace.com/static/55a0c7c1e4b0ab3ab0407073/t/5c2960d32b6a28bef1a2dea9/1546215658386/BWTcover.jpg?format=1500w)

Part of the appeal in being a worker on new gig-economy platforms like Uber or Taskrabbit is the apparent autonomy, the feeling of not having a boss. Sure, an app on your phone is your new boss, and through it a large, transnational corporation whose investors want nothing more than to automate you away, but maybe that beats someone coming out of the corner office to breathe down your neck. For some people, the app-boss is at least a step in the right direction.

Toward what? Most of us probably aren't sure. But the people involved in a Wellington, New Zealand-based network called Enspiral have done more than just about anyone to figure out---to figure out where we'd want the future of work to be headed if the better angels of our nature were in charge. I've had the chance to visit them (and [lived to tell the tale for _Vice_](https://www.vice.com/en_us/article/ava37e/figuring-out-the-freelance-economy-v23n6)). Now, a trip down to Wellington, although I absolutely recommend it, is a little less necessary. The Enspiralites have created a book, _[Better Work Together](https://betterworktogether.co/)_, which chronicles in conversational stories and pictures their attempts to create a kind of community worth working toward.

Enspiral is fairly small, as organizations go---a few hundred active participants, a modest budget. Rather, it's lean. Most of the Enspiralites' businesses exist outside the organization, but attached to it, allowing Enspiral itself to take risks, learn lessons, and reinvent itself when necessary. It's a community of early adopters. They offer themselves as beta-testers for a suite of collaboration software they've co-produced, such as [Loomio](https://loomio.org) and [Cobudget](https://cobudget.co/). They relentlessly explore challenging governance frameworks like sociocracy and teal. They even funded the book's production through a new blockchain-enabled platform called [DAOstack](https://daostack.io/) (which still crashes my browser when I try to use it). These are not ordinary workers; they're people with the passion, the patience, in many cases the privilege, and the fault-tolerance to repeatedly try stuff that may or may not work.

In the book, you'll see why. There is a generosity and pleasure and even a spirituality in how they talk about their efforts that makes it all seem less like, well, work. There are typos, but these pale in comparison to the challenges we collectively face. The upshot is not a final theory or doctrine or destination, but a mode of working toward it, of declining to accept disguised versions of feudalism as good enough. [Order it, digitally or physically, here](https://betterworktogether.co/).
