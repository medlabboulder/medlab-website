---
layout: event
title: "Exit to Community: A New Option for Startups?"
summary: "What if startups could transition to ownership by the people who rely on them most?"
tags: [internet-of-ownership, events]
event-date: December 11, 2019
event-time: 10-11:30 a.m. Mountain Time
event-location: Webinar
---

<img alt="Pricing table for startup exit options" src="{{ site.baseurl }}{% link /assets/e2c-pricingtable.png %}" style="width:100%" />

When a startup company takes early investment, typically the expectation is that everyone is working toward one of two “exit” events: selling the company to the investor-owners of a bigger company or selling to stock-market investors in an initial public offering. What if there were a third option, an "exit to community," in which a startup transitions to ownership by the people who rely on it most?

Those people might be users, workers, customers, participant organizations, or a combination of such stakeholder groups. The mechanism for co-ownership might be a cooperative, a trust, or even crypto-tokens. The community might own the whole company when the process is over, or just a substantial-enough part of it to make a difference. These kinds of outcomes could help prevent the accountability crises that now beset today’s most successful venture-backed startups.

In this participatory webinar, we'll hear from entrepreneurs, investors, and activists who are working to make exit-to-community a viable option in the startup economy. We will also work together to devise some plausible pathways for how such exits might become a reality.

<iframe src="https://archive.org/embed/exit-to-community-webinar-20191211" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

## Speakers

* Camille Canon (Purpose Network)
* Bruno Haid (founder, Roam)
* Scott Heiferman (founder, Meetup)
* Arielle Jordan (founder, Curafied)
* Jonathan Moore (founder, RowdyOrb.it)
* Modupe Odele (attorney, Tiphub)
* Mara Zepeda (founder, Switchboard and Zebras Unite)

## Read more

* "[Startups Need a New Option: Exit to Community](https://hackernoon.com/startups-need-a-new-option-exit-to-community-ig12v2z73)," *Hacker Noon* (September 16, 2019)
* "[Meetup to the People: How a Zebra could Rise from a Unicorn’s Fall](https://medium.com/@sexandstartups/meetup-to-the-people-how-a-zebra-could-rise-from-a-unicorns-fall-cfa93d83bcdc)," @sexandstartups on Medium (November 5, 2019)

*Hosted by the [Media Enterprise Design Lab](http://cmci.colorado.edu/medlab/) at the University of Colorado Boulder and [Zebras Unite](https://www.zebrasunite.com/), with support from the Open Society Foundations, which are not responsible for the content.*

<img alt="Zebras Unite" src="{{ site.baseurl }}{% link /assets/zebras_unite_logo.jpg %}" style="width:100%" />


