---
layout: post
title: "Hypothesis: ESOPs for the Online Economy"
summary: Introducing a research project on user-owned trusts
author: Nathan Schneider
tags: [collab-gov, internet-of-ownership]
---

I work in a startup town, the rare kind of place where you can trip over veteran founders, with multiple exits behind then, on the sidewalk. By "exits," I mean the end-goal of most tech-oriented startups---the moment when the startup is sold, either to a bigger company or, more rarely, to the investing public on the stock market. The whole culture of startup communities like Boulder is aimed toward this; it's when founders and investors get their big payday. And yet this is the logic that turns our online infrastrutures into commodities. In the exit, it is often the data and loyalty of us the users that is being sold to the highest bidder.

What if another kind of exit were possible? What if founders and investors could aim for an exit that sold to the users with a real stake in the future behavior of the firm, as well as in its sustainability? These are questions I've been puzzling on for some time now, and I think I'm starting to see a viable path forward.

The ESOP, or employee stock ownership plan, is the most successful strategy for enabling employee ownership in the US economy. In companies from New Belgium Brewing to Southwest Airlines, it makes worker-owners of 14 million Americans. In contrast, there are just several hundred worker co-ops with just several thousand workers among them. Two things make the ESOP model work as well as it has:

* it consolidates the ownership shares in a trust, making it far easier to finance than a bunch of individual workers with individual credit histories
* since the 1970s and 1980s, there has been appropriate tax treatment in US law

For a particular profile of firms (generally closely held and medium-sized) the ESOP has been an attractive exit strategy for many founders.

The inventor of the ESOP model, Louis Kelso---a Coloradan and graduate of CU Boulder's business and law schools---didn't want to stop at that. He envisioned the ESOP as just one kind of "SOP" enabling more broad-based capital ownership. I suspect the challenges of the online economy present an opportunity to consider other such models he proposed, particularly the CSOP, or consumer stock ownership plan. Analagous to the ESOP, this would enable long-term users of a business (Kelso envisioned examples like neighborhood grocery stores or monopoly utilities) to become owners of it---by borrowing outside capital on the promise of future growth. Such financing means the CSOP could provide an exit payout far greater than what cash individual users could muster.

What if, for instance, Uber drivers could become dividend-earning owners in this way, or if Facebook users could use such a trust to elect their own trustees to the company's board? This could be a strategy for remedying some of the critical accountability crises of the online economy.

I am currently engaged in research on the feasibility of such models, with the support of a fellowship from Rutgers University's [Institute for the Study of Employee Ownership and Profit-Sharing](https://smlr.rutgers.edu/content/institute-study-employee-ownership-and-profit-sharing). The first step is a paper with [Morshed Mannan](https://www.universiteitleiden.nl/en/staffmembers/1/morshed-mannan), a brilliant thinker on legal strategies for a more democratic online economy. We're exploring what conditions would work best for this kind if exit, as well as the policies needed to make it more feasible.

More to come. [Here's a small bibliography-in-progress](https://www.zotero.org/ntnsndr/items/collectionKey/U298EBUI). In the meantime, if this topic is related to your interests, I would love to hear from you.
