---
layout: post
title: Notes on Collective Governance
author: Katy Fetters
summary: What can collective governance look like? What shape does that take?
tags: [collab-gov]
---

<img alt="Wave image by Katy Fetters" src="{{ site.baseurl }}{% link assets/fetters-wave.jpg %}" style="float:right; width:50%; padding:10px;" />Those of us looking to shape our enterprises with methods for collective governance and shared ownership are led to ask: *What can collective governance look like? What shape does that take? What are some of the challenges and freedoms presented in this model?*

We spoke with several cooperative-minded experts who offered their insight into these questions, as part of a collaboration with the Action Network, a nonprofit online mobilization platform whose team is seeking to further democratize its operations. Here are a few takeaways from our discussion:

* **Clarity:** Facilitate strong and clear communication around your offerings and core values. Create a ladder of engagement for the purpose of empowering members around co-ownership, leadership, and responsibility. The earlier you can define members' roles and emphasize their level of commitment, the more assured your members and core partners may feel.
* **Trust:** Build trust within your organization to increase loyalty and engagement among members and core partners. It will be difficult to be everything to everyone. 1) Recognize the nature of your member-base; if they are mostly international, allow for participation in voting and web-based meetings based on remote positioning and convenience to their time zone. 2) Record and send out online meetings, creating open discussion forums and providing a variety of ways in which your members can communicate their needs and opinions to you. 3) Develop tools to increase accessibility and transparency around important stakeholder information, and show your members how to engage with your platform.
* **Collaborative effort for collective success:** Understand that people respond differently to different kinds of communication. Some will want to dominate much of the activity or conversation on the platform. Spot the need to activate or incentivize many members to engage based on their preference or ability so that the contribution of value and insight is more fairly distributed. 
* **Use your users:** In any early stage of a startup or software project, the best test group for your product is your members; give room for critical feedback as a chance to listen, learn, and improve. This will also cultivate an environment of oppeness and transparency at every level of the process, while also retaining the agility and autonomy that the development team needs.

If we think about a wave—the flow, the tide that pulls from within, it requires many forces working in tandem to build momentum and energy, enough to create the wave’s body and crest. 

*A special thanks to the participants in this discussion: Brian Young (executive director and founder, Action Network), Martha Grant (product manager, Action Network), Alanna Irving (team member, Open Collective; co-founder, Enspiral and Loomio), Chris Tittle (director of organizational resilience, Sustainable Economies Law Center), Margaret Vincent (senior counsel, Stocksy United).*
