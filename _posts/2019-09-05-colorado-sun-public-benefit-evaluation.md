---
layout: post
author: Laura Daley
title: "How MEDLab Evaluated the Colorado Sun's Public Benefit"
summary: 'The search for a third-party standard in journalism'
tags: [stakeholder-news, shared-ownership-in-colorado]
---

[*The Colorado Sun*](https://coloradosun.com/) is a new Colorado
news organization seeking a business model that allows it to break
free from the outside financial interests [currently dominating the
state's news scene](https://www.youtube.com/embed/uR_jBlB-fj8?hd=1). Founded as an employee-owned company just last year,
the *Sun* has incorporated as a Public Benefit Corporation---a
categorization for companies that value social or environment impact
alongside turning a profit. Maryland led the charge in PBC legislation
in 2010 and since then, quite a few states have followed suit. While
PBCs don't receive specific tax benefits, their status illustrates a
strong commitment to giving back to the public.

Different states require different actions on the part of a PBC to
demonstrate its success in achieving the chosen benefits. In
[Colorado](https://www.sos.state.co.us/pubs/business/FAQs/pbc.html),
PBCs are required to publish an [annual
report](https://codes.findlaw.com/co/title-7-corporations-and-associations/co-rev-st-sect-7-101-507.html)
with a narrative description of their progress: What went well? What
challenges hindered achievement of the benefit? The report is also
supposed to include an evaluation by a third-party standard.

*The Colorado Sun* incorporated as a PBC in January. This August, then,
was its first opportunity to complete the annual report. The *Sun*
staff asked MEDLab to provide consultation on the reporting procedure. (There
was no fee or other financial relationship associated with this arrangement; 
MEDLab has received a grant from the Brett Family Foundation to support work
on Colorado journalism.)

Our process began with research about PBCs. In Colorado, annual
reports are required to be published on a company's website or available
to anyone who for them; how hard could it be to find examples to draw
on?

We found lots of example reports, but many were wholly irrelevant to the context of journalism or media production in the state. In-depth, beautiful reports from companies like [Namasté
Solar](http://info.namastesolar.com/hubfs/2019_Public_Benefit_Report_Namaste_Solar.pdf)
provided guidance for tone, format, and breadth, but the standards those
companies used didn't necessarily apply to the *Sun.* For instance,
environmental impact may be easy to quantify for companies creating consumer
goods (or, like Namasté, installing solar panels), but that task is harder for an online-only news platform.
Standards like those used by the nonprofit certifier B-Lab can be
useful, but questions about employee benefits and charitable giving
don't quite capture the community impact of a news organization.

How can one assess the impact of the news? Terms like "democracy" and
"public knowledge" seemed too nebulous, too big to attach to tangible or
measurable actions by the *Sun*. We had to improvise.

Our first step was [creating a questionnaire](/medlab/assets/SunEvalQs-2019.pdf) of reflective questions for the *Sun* staff members. We wanted to hear
from them about how they evaluate themselves, and we began
with questions drawn from the *Sun*'s own purpose statement in its
articles of incorporation. We also drew on the Society of Professional
Journalists' [code of ethics](https://www.spj.org/ethicscode.asp) and
the [journalism ethics
policy](https://civil.co/constitution/#ethics-policy) of Civil, the
blockchain-based network of news organizations of which the *Sun* is a
founding member.

The *Sun* responded with incredibly mindful, intentional responses that
far exceeded our expectations. Staff members shared data sets
illustrating not only audience reach and geographic coverage but also
tangible examples of instances when the *Sun*'s reporting had clear
impact. With these in hand, we undertook the next step of the process:
MEDLab director Nathan Schneider
[interviewed](https://news.kgnu.org/2019/08/looks-like-new-what-is-public-benefit-journalism/)
Dana Coffield on our KGNU radio show [*Looks Like
New*](https://cmci.colorado.edu/medlab/radio-podcast-looks-like-new/).
The conversation further illuminated the *Sun*'s purpose and impact, and
it was broadcast across the Front Range.

Following the interview and the responses to our questionnaire, we
created an
[evaluation](/medlab/assets/SunEval-2019.pdf), which in turn the _Sun_ included in its first [annual report](https://coloradosun.com/annualreport).

The *Sun*'s achievement of its stated benefit purpose was remarkable. The shortcomings we identified are largely consequences of its early-stage limitations. Because of its small staff size, for
instance, senior editors are currently involved in business and
sponsorship decisions. Securing the sponsorships aspired to in the
purpose statement has also been a challenge for the *Sun*, so grant funding
and memberships have been the main sources of revenue. The *Sun*, like
many news organizations, also struggles to reflect the diversity of the
community in the diversity of its newsroom.

The standard we created to evaluate public benefit journalism was
multifaceted, as it was based on "hard" numerical data as well as "soft"
anecdotal evidence. The *Sun*'s mission is to provide accessible news
for the entire state; we took a look at the kinds of coverage they
provided for all parts of Colorado---including the areas that often go
unacknowledged by larger, Denver-based publications. In the coming years,
the *Sun* can analyze changes in this data to identify any gaps in both coverage and audience locations around the state. The other important aspect of
our quantitative standards focused on the business model of the *Sun*
through exploring the breakdown of funding sources and their
relationship with the *Sun*'s purpose statement.

But numbers alone don't give justice to the *Sun*'s impact. We
identified standards to evaluate each clause of the *Sun*'s purpose
statement and probed them with questions related to the tenets of
journalism. These standards focus on ethics, accuracy, independence,
truthfulness, and contributions to Colorado's democracy. We asked process-oriented questions whose answers are hard to measure quantitatively: How does the *Sun* verify information
and sources? Parse opinion and commentary from fact? Fairly represent
sources and relevance of information? How does the Sun maintain
independence? Avoid conflicts of interest? Interact with advertisers,
donors, and sponsors?

Impact is no doubt difficult to measure, but this
hybrid approach for understanding all parts of the *Sun*'s mission and
fulfillment of it hopefully yields an accurate depiction of the publication's first year. We also hope that what we have done will be useful to other news organizations seeking to evaluate their public benefit. In the long term, however, it is probably best that specific standards from journalism simply inform how journalism PBCs fill out a more standard, cross-industry instrument like the [B Impact Assessment](https://app.bimpactassessment.net/login).

Public Benefit Corporation status is an emerging strategy for better
reflecting the dual role of journalism as a public service and a
business. It may help to protect local news from the profit-seeking,
outside interests that have dominated newsrooms in Colorado and
throughout the country. *The Colorado Sun* is a pioneer in this, and we
have been grateful to help document its progress.
