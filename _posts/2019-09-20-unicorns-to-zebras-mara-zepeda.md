---
layout: event
title: "The Future of Startups, from Unicorns to Zebras"
summary: A conversation with Mara Zepeda of Zebras Unite
tags: [internet-of-ownership, events]
event-date: October 4, 2019
event-time: 3 - 4:30 p.m.
event-location: CASE building, rm. E422   
---

<a href="{{ site.baseurl }}{% link /assets/ZepedaEvent.pdf %}"><img alt="Event poster with Mara Zepeda" src="{{ site.baseurl }}{% link /assets/ZepedaEvent.png %}" style="width: 100%;" /></a>

**[See photos from the event here](https://cloud.medlab.host/s/qPo9rSdyJXQFgyi)** by Kimberly Coffin 

Startups are increasingly looked to as the future of the economy, but their founders, their investors, and the people they chiefly serve often represent a narrow, privileged minority. Mara Zepeda is working to change that. As CEO of her own startup, Switchboard, and co-founder of the Zebras Unite network, she is opening the doors of startup finance and culture to a much broader range of founders and communities. Come for an open conversation on how to foster truly inclusive entrepreneurship.

Coffee and snacks provided. [Please RSVP here](https://cloud.medlab.host/apps/forms/form/UqTEVdpL5gZEkcHw).

*Hosted by the Media Enterprise Design Lab in the College of Media, Communication, and Information.*