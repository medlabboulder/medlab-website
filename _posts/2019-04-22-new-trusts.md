---
layout: event
author: Nathan Schneider
title: "The New Trusts: Democratic Ownership Beyond the ESOP"
summary: A webinar on emerging explorations on trust structures—their promise and their problems
tags: [internet-of-ownership, events]
event-date: June 6, 2019
event-time: 10 a.m. - 12 p.m.
event-location: webinar
---

<iframe src="https://archive.org/embed/new_trusts_democracy_ownership_beyond_ESOP" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

The employee stock-ownership plan, or ESOP, is one of the most powerful legal innovations in recent US history. Since its introduction in the mid-1970s, millions of employees have benefited from being co-owners of the companies where they work. But ESOP formation has slowed in recent years, and it has come with shortcomings. A new generation of lawyers, investors, and worker advocates has been exploring new strategies for broad-based ownership—potentially serving employees, contractors, and community stakeholders. Many of these involve reworking the mechanics of the trust, the legal structure at the heart of the ESOP.

In this interactive webinar, we'll hear from some of the leaders of this emerging conversation about the promise and pitfalls of new strategies for democratic ownership.

### Introduction

Nathan Schneider (University of Colorado Boulder)   
Advancing the Kelso paradigm, from ESOPs to online platforms

### Provocations

Christopher Michael (Christopher Michael P.C.)   
Perpetual trusts for employee ownership

Camille Kerr (Upside Down Consulting)   
Labor trusts for worker benefit

Derek Razo (Purpose Ventures)   
Purpose trusts for multi-stakeholder stewardship

### Responses

Sara Stephens (Sustainable Economies Law Center)   
Lessons from the legacy of land trusts

Jason Wiener (Jason Wiener|p.c.)   
Uncomfortable questions about trust structures

Followed by open discussion

*Hosted by the Media Enterprise Design Lab at CU Boulder's College of Media, Communication and Information, with support from the university's Office for Outreach and Engagement and the Brett Family Foundation.*
