---
layout: post
author: Nathan Schneider
title: "Co-ops, from Lateral to Vertical and Back Again"
summary: Ask not what your co-op can do for its members but what they can do for each other
tags: [collab-gov]
---

<img alt="Alphonse Desjardins (photo by Vista Stamps, used without permission)" src="{{ site.baseurl }}{% link /assets/desjardins.jpg %}" style="width: 100%;" /><br />
<em>Alphonse Desjardins (photo by Vista Stamps, used without permission)</em>

I have been trying for some time to put my finger on the difference---between the stories of co-op origins I read about for years while working on my book, _[Everything for Everyone](https://nathanschneider.info/e4e)_, and what I encounter among many larger cooperatives today. Certain differences are obvious: the scale, the maturity, the technology, the political power, and the global reach of co-ops now are well beyond what the founders could have imagined a century or so ago. But many co-ops have also lost a feature that was their main competitive advantage at their founding.

To me, this has been a useful way to put it into words: Co-ops have traded lateral relationships for vertical ones.

I could take just about any example of a great co-op's origins, but the account I've most recently read is _The Poor Man's Prayer: The Story of Credit Union Beginnings_ by George Boyle, a somewhat fictionalized biography of Alphonse Desjardins, the founder of the North American credit union movement, published in 1951. Two significant patterns jump out in the story, patterns that are utterly typical in the genre of co-op origins.

First, the leadership of Desjardins and his family is critical; great co-ops tend not to emerge from amorphous groups, [as I've seen many new cooperators assume](https://cmci.colorado.edu/medlab/2019/01/20/co-ops-need-leaders-too.html), so much as from visionary leaders able to bring groups into being. It is through those groups that the co-op becomes truly powerful. How? The second pattern is that the early, great co-ops grew out of what members can do for each other. Through member-to-member self-help, they were able to fill [missing markets](https://www.tse-fr.eu/sites/default/files/medias/doc/conf/workshop_po/communications/brent_huet.pdf) and compete with better-financed capitalists.

This is not a utopian, rose-colored-glasses idea. It doesn't mean giving up on appropriate hierarchy within the business. It rarely involves holding hands in circles (except in a rather conventional place of worship). It just means that co-ops find competitive advantages in the member ties they build upon and cultivate. In the case of Desjardins's *caisse populaire*, the secret was having parish-level groups of members who knew each other evaluating loan applications. Through longstanding relationships, members could determine creditworthiness in communities where conventional banks couldn't tell a follow-through-er from a cheat.

The conversations I hear among credit union executives today is somewhat different. On the whole, these are people who want nothing more than to do good for their members. They compare notes with each other about new products and services that might make their members' financial lives better. They take their mission-focused, nonprofit model seriously and always want to improve it. But they rarely discuss the very strategy that built their institutions: relationships among members.

The question, that is, has become what service *x* the co-op can offer, especially when there's a way to get economies of scale by multiplying *x* by *n* members. Still, the assumption is basically that *x* serves a unitary, isolated member whose sole relevant relationship is with the co-op itself.

This shift from lateral (member-member) to vertical (institution-member) relationships is something that co-ops came by honestly. It accrued over time thanks to living in a world increasingly dominated by capitalist business, which wants nothing more than for the market to be made up of isolated, vulnerable individuals with no collective power or competence. The regulations that co-ops have to follow were written with this logic in mind. The technologies that co-ops feel pressured to adopt have this logic hard-coded into their designs.

Ironically, however, capitalists have been discovering the power of the lateral. "Sharing economy" platforms purport to connect users to each other. Crowdfunding platforms enable people to pool small-dollar contributions like co-ops once did. Social media platforms depend almost entirely on the content that users create for each other. Co-ops have fallen behind in the very lateral game that was once their edge.

Of course, there is a difference between the lateral relationships of capitalist platforms and the kind that co-ops can make possible. The "sharing" platforms have been increasingly recognized as extractive, often monopolistic ploys. And the accountability of co-ownership that co-ops enable is of a kind beyond what mere crowdfunding campaigns or social-media likes can offer. There are still tremendous opportunities for relearning the competitive advantages of lateral cooperation; it just means remembering the business patterns that have always made co-ops distinct. Perhaps this also means relinquishing the vertical turf on which co-ops will usually be out-capitalized and outdone by investor-owned competitors.

Today, however, is not a century ago. The next lateral cooperativism needs tools for greater scale and dynamism than the parish-level boards that Desjardins organized. I have been seeing signs of what this return to the lateral might look like. Here are some of the examples I've noticed:

* Purchasing co-ops have begun building marketplaces that allow members to buy from each other and choose what to buy together, rather than relying only on the selections of co-op staff
* The [Filene Research Institute](https://filene.org) has found that when credit union members come together for focus groups on products, they start sharing financial advice with each other
* [We Own It](https://weown.it/) (on whose board I sit) works to improve co-op governance not just by educating staff and directors but by supporting efforts by members to organize reform campaigns
* The [P2P Foundation](https://p2pfoundation.net)'s recent work on accounting systems and open-value networks suggest how emerging tools might facilitate member-to-member economies

Co-ops of the near future could find themselves asking less what they can do for their members and more what their members---and potential members---can do for each other. This might include co-ops facilitating peer-to-peer lending and mutual credit systems among their members, for instance, or online forums and marketplaces that help members build ties and meet needs together. Through the trust and relationships these kinds of strategies generate, lateral business can lead to more lateral governance. As more of a co-op's business happens through member initiative, it will be only natural to explore new approaches for incorporating member voice into decision making. Members can help guide their co-ops into the missing markets that the capitalist competition doesn't know how to notice.

Is the return of lateral power something your co-op is interested in being part of? We at MEDLab would like to help.
