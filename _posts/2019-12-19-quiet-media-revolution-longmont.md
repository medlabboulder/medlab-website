---
layout: post
author: Cassandra Dana
title: "The Quiet Media Revolution in Longmont"
summary: 'Many in Longmont are excited to be pioneering their own model for public
access television.'
tags: [stakeholder-news, shared-ownership-in-colorado]
---

When the *Longmont Observer* responded to a request-for-proposals for control over the
city's public access channels, the team wasn't sure they'd win. The contract
had been held by another organization for over 30 years, and most
assumed it was unlikely to change hands. But the *Observer* presented an
unusual idea, one that sparked imaginations by proposing to reconfigure
notions of media ownership.

If you walk into one of Longmont Public Media's community organizing meetings, you'll find 20 to 30
enthusiastic individuals happy to greet you and excited to work. Longmont Public Media is the *Observer*'s new public
access branch. The energy of this eclectic group is palpable.
Many in Longmont are excited to be pioneering their own model for public
access television.

The notion of public access television was devised in the late 1960s and
early 1970s. In the era of the first televised war, NBC, CBS and ABC
controlled broadcast coverage of the cultural, political and social
conflict. They depicted the gruesome fighting overseas and the civil
unrest here in the United States. The effects were extraordinary. Screen
media began to have a major influence on public policy and mass
upheaval. TV began to be seen as a uniquely powerful force, one that was
dictated by commercial broadcast networks. Recognizing the power of the
medium, counterculture movements and media scholars began to advocate
for what was referred to as "[Guerrilla Television](https://en.wikipedia.org/wiki/Guerrilla_television)." Michael Shamberg
pioneered the term in 1971, stating, "Guerrilla Television is grassroots
television. It works with the people, not from above them."

Recognizing the importance of community driven media, the FCC developed
regulations authorizing state and local governments to require cable
television networks to set aside channels for public access. Cable
companies entered franchise agreements with municipalities in which
access to infrastructure——telephone poles, sidewalks, etc.——was granted
in exchange for 5 percent of companies' gross revenue. This franchise
fee was paid to the city, which often reinvested this money into public
access. Initially many local municipalities adopted this model, granting
community members access to production and distribution methods.
However, as pressure for public access decreased and many municipalities
began experiencing increased financial burdens, funding for public
access became less feasible. Some communities shut down public access
channels entirely, while others limited them to local legislative
proceedings. Public access shifted from a place where community members
could develop skills and exhibit ideas to a droning of traffic court and
city council hearings.

To complicate the matter further, in August 2019 the FCC approved an
amendment to their 1970s regulation. The change in legislation allows
cable companies to deduct "in-kind provisions" from their franchise
fees. This means cable companies can now assert that the market value of
public access networks is deductible from
the revenue they pay to the municipalities. In 2017 Longmont allocated
25 percent of the franchise fees collected from Comcast (or \$187,924)
to public access television. [According to the _Colorado Sun_](https://coloradosun.com/2019/02/11/cable-franchise-fee-limit-public-access-channels/), that is
enough funding to cover one full-time employee and two part-time
employees. The diminishment of franchise fees will seriously impact
the ability of cities and states to maintain public access television.
The president of Rocky Mountain PBS told the _Colorado Sun_, "Going from
having franchise fees to not having them, typically what happens in that
case is the public access goes away." It's clear that new models must be
developed in order to maintain public access.

Longmont Public Media has taken on the challenge of innovating public
access. Its founders have suggested that public access television can truly
function as it was intended, as a resource created for and by the
masses. They have proposed a cooperative model of media ownership in
which members pay a small fee and in exchange can produce and exhibit
their work. The studio that houses Longmont Public Access will be
transformed into a media makerspace, serving as a venue for community
members to create, collaborate, share infrastructure and distribute
work. Each member of the co-op contributes to ideas around governance,
programming, events and space utilization. As Michael Shamberg explained
when he was proposing public access, "The inherent potential of
information technology can restore democracy in America if people become
skilled with information tools."

This is not Longmont's first foray into municipal ownership of community
media. In 2014 Longmont launched NextLight, a municipally owned
broadband enterprise. NextLight has dethroned Google Fiber as the
fastest fiber-optic network, and it is now a national model for publicly
owned internet access. Five years later, Longmont is building on this
precedent through the creation of Longmont Public Media.

When Longmont Public Media asked MEDLab to get involved——to help
formulate this model of cooperative public access——I knew it was an
opportunity not to be missed. Thursday nights have become one of my
favorite parts of the week, when I slide past the local middle school
choir and the couples quietly nestled sipping lattes to the back room of
the local coffee shop, where we work to revolutionize media ownership.
