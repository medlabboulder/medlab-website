---
layout: event
title: "Exit to Community: Community Culture"
summary: "Learn about community culture from two specialists in digital organizing"
tags: [internet-of-ownership, events]
event-date: March 6, 2020
event-time: 10-11:00 a.m. Mountain Time
event-location: Webinar
---

How can startups create truly empowered communities? A growing network of entrepreneurs, organizers, and investors is exploring the idea of “Exit to Community”—enabling startups to transition toward ownership by their core stakeholders. There are many possible pathways towards this goal, as well as many challenges along the way. But one way or another, community ownership depends on community culture.

In this webinar, we will learn about community culture from two specialists in online communities and digital organizing. We’ll talk about why community stakeholders are the toughest customers, how to engage them in building power, and what the key tensions are around governance, control, and making things work. After brief presentations, we’ll dedicate most of our time to a facilitated discussion based on questions from webinar participants. Expect to come away with strategies for your own Exit to Community.

**[RSVP here](https://docs.google.com/forms/d/1DCiVv9HdYDZi1CFs9Ypo-uQR6jmiieiUeplZHFBB2ZY/viewform) to share your questions about community culture.**

## Speakers

* Carrie Melissa Jones ([Gather Community Consulting](https://www.gathercommunityconsulting.com/))
* Ari Trujillo-Wesler ([OpenField](https://openfield.ai/))

## Resources

* “[Startups Need a New Option: Exit to Community](https://hackernoon.com/startups-need-a-new-option-exit-to-community-ig12v2z73),” *Hacker Noon* (September 16, 2019)
* “[Instead of IPOs and Acquisitions, Exiting to Community Is One Alternative](https://cloud.medlab.host/s/ACcHwG5TodRwxdL),” TechCrunch (February 25, 2020)
* "[Exit to Community: A New Option for Startups?](https://cmci.colorado.edu/medlab/2019/12/01/exit-to-community-webinar.html)," MEDLab webinar (December 11, 2019)
* “[Meetup to the People: How a Zebra could Rise from a Unicorn’s Fall](https://medium.com/@sexandstartups/meetup-to-the-people-how-a-zebra-could-rise-from-a-unicorns-fall-cfa93d83bcdc),” @sexandstartups on Medium (November 5, 2019)
* "[Conversion to Accountability: Strategies for Multi-Stakeholder Ownership in the Platform Economy,](https://docs.google.com/document/d/1SFUklZmxDHVKU-fXlXc8C5JDmPuAJU9CN7Oklgjghck/edit#heading=h.mih5fqhxywjl)" working draft

*Organized by Danny Spitzberg, community fellow with the Media Enterprise Design Lab @ CU Boulder, in partnership with Zebras Unite, and with support from the Open Society Foundations (which are not responsible for the content).*

<img alt="Zebras Unite" src="{{ site.baseurl }}{% link /assets/zebras_unite_logo.jpg %}" style="width:100%" />


