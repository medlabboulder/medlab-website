---
layout: post
author: Nathan Schneider
title: "Startups Need a New Option: Exit to Community"
summary: "Why are we building disposable companies?"
tags: [internet-of-ownership]
---

<img alt="Pricing table for startup exit options" src="{{ site.baseurl }}{% link /assets/e2c-pricingtable.png %}" style="width: 100%;" />

Founders create startups for all sorts of reasons. Often, the motivation
is a mix between the founders' desires to do well for themselves and to
do something worthwhile for others. Dreams of greatness might figure in
there too. Rarely, however, is the overriding reason to build a company
people want to get rid of. But that is what the startup pipeline is
designed to produce.

When a startup company takes early investment, typically the expectation
is that everyone is working toward one of two "exit" events: selling the
company to a bigger company or selling to retail investors in an initial
public offering. In either case, the startup is a hot potato. One group
of investors buys in order to sell to another group of investors who buy
in to sell to the fools down the road. There's something sort of
pyramid-scheme-ish about all this. The exit event, also, is often the
beginning of the end of any positive social vision that the company
might have held.

What if startups had the option to mature in a way that gets them out of
the investors' hamster wheel?

In the coming months, I will be exploring strategies and stories that
could help create a new option for startups: Exit to community. In E2C,
the company would transition from investor ownership to ownership by the
people who rely on it most. Those people might be users, workers,
customers, participant organizations, or a combination of such
stakeholder groups. The mechanism for co-ownership might be a
cooperative, a trust, or even crypto-tokens. The community might own the
whole company when the process is over, or just a substantial-enough
part of it to make a difference.

When a startup exits to community, founders should see enough of a
reward that they feel their risk and hard work was worth it. Investors
should see a fair return for their risk. Most importantly, the key
stakeholders should know the company is worthy of their trust and
ongoing investment because they co-own it. For a social-media company,
this might mean that users have a meaningful say in how their private
data is or isn't used. For a gig platform, it might mean that the gig
workers co-determine their working conditions and what is done with the
profits they produce. These kinds of outcomes could help prevent the
massive accountability crises that now beset today's most successful
venture-backed startups.

One way to begin exploring E2C could be by identifying a subset of
startups in venture capital portfolios that lie in "zombie"
territory---somewhere between failure and exit-ready. Investor owners
would benefit from having a new way of liquidating investments that
would otherwise lie dormant. In some cases, the community might be in a
position to buy the company with cash on hand---especially if it came
back to them in later savings or profits. In other cases, E2C might be
financed externally on the expectation of future growth, as is generally
done for employee-ownership conversions using an Employee Stock
Ownership Plan. Startups might also plan ahead for E2C by identifying
particular guardrails that keep this option open as they negotiate their
early rounds of financing. As with the ESOP---and with [the venture
capital industry
itself](https://logicmag.io/scale/the-unicorn-hunters/)---a targeted
policy intervention may be necessary to make this kind of financing
attractive enough to be feasible. These possibilities and more are the
kinds of things I've been thinking about and would like to think about
with others.

Why not, you might ask, just begin these startups under community
ownership? This is certainly an option, and it's one that I have
enthusiastically supported through the \#PlatformCoop community and
through co-founding the Start.coop accelerator. But getting going under
community ownership doesn't seem like the right approach in many cases.

Ambitious startups are a risky endeavor, and it may not be fair to
distribute that risk with early-stage participants. Also, startups
usually need to make a few dramatic pivots early in their life, and
having a large community of co-owners would make those hard decisions
more difficult than if a small, high-trust group of founders is in
charge. Centralizing the risk and responsibility early on is a
reasonable strategy for startups. Later, once the company has found its
market and its footing, the transition to accountable community
ownership will better suit the nature of the business. With E2C, we get
the best of both worlds---the dynamic startup, then the accountable,
sustainable public asset.

For me, this vision came together in conversations with social
enterprise lawyer [Jason Wiener](http://jrwiener.com/team/jason/) (who
has participated in some exits to community), along with sources of
inspiration that include [Zebras Unite](https://www.zebrasunite.com/),
[Louis
Kelso](https://osf.io/v7fe2/?view_only=2ffd750b4ba54001beb5a459d61faff0),
[platform cooperativism](https://platform.coop/), and the
[steward-ownership](http://steward-ownership.com/) network. Now it is
time to bring more people into the conversation.

Our team at the [Media Enterprise Design
Lab](http://cmci.colorado.edu/medlab/) at the University of Colorado
Boulder is looking for collaborators on this work. This includes
entrepreneurs, activists, investors, policy advocates, researchers, and
more. Do you want to join us? [Let
](mailto:medlab@colorado.edu?subject=E2C)[us](mailto:medlab@colorado.edu?subject=E2C)[
know](mailto:medlab@colorado.edu?subject=E2C) what you'd want your E2C
to look like.

*Also published at [Hacker Noon](https://hackernoon.com/startups-need-a-new-option-exit-to-community-ig12v2z73) and [The Internet of Ownership](https://ioo.coop/2019/09/startups-need-a-new-option-exit-to-community/).*