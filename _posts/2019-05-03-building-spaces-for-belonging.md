---
layout: post
author: Katy Fetters
title: "Building Spaces for Belonging"
summary: '"I’ve come to realize that I have to create an organization"'
tags: [collab-gov]
---

**Excerpt—[Read the rest on our Medium page](https://medium.com/medlab/building-spaces-for-belonging-7024f49ab071)**

<img alt="Katy Fetters" src="{{ site.baseurl }}{% link /assets/katy_fetters.png %}" style="float:left; width: 200px; padding: 0 10px 10px 0px;" />In recent years, I have struggled to call myself an advocate for those with cerebral palsy. I’ve struggled to understand what that means both online and offline in the disability community.

What do I advocate for? Whom do I advocate for?

While attempting to answer these questions, I’ve come to realize that I have to create an organization.

Cerebral palsy is a congenital brain injury that manifests itself in a variety of ways, in different regions of the body. In my case, the left side of my body is affected and so I experience hemiparesis — limited movement and development in most of my muscle groups. Atrophy, weakness, and spasticity have left me with a distinct limp, and so I wear a sophisticated leg brace that improves my stamina, mobility and overall quality of life. I was born with CP, I grew up with CP, and will grow old with CP. Yet this fact is something that is not widely acknowledged by medical professionals and disability organizations. CP is usually described as a childhood disability, but what happens when children grow into adults? They don’t simply grow out of their disability.
image by blnk films

Since 2009, I have blogged sporadically about my life experiences with cerebral palsy. Initially, I named this site TeenCP but as I outgrew this phase of life, I started anew with [Cerebral Palsy Strong](http://www.cerebralpalsystrong.com/) which captures our hashtag, [#cpstrong](https://www.instagram.com/explore/tags/cpstrong), on social media as well as a broader portion of individuals with the disability. This is a growing online community consisting of mostly young adults with cerebral palsy who want to take part in our movement to change the way others view disability; to call for a different kind of support and locate a sense of belonging within this space.

I’d begun to notice that there was and still is a vast desire among young people with CP to ‘meet’ others; to connect, relate, and share information that could help those around us. This desire stems from a deep, even subconscious understanding early on in life that our disability somehow makes us different from our friends and family. I didn’t have the words at 8 years old or an awareness at 11 to express that but I knew sharply, that the way people stared at my legs or the fact that I had to go to physical therapy appointments set me apart from even the person most like me — my identical twin. And it might take years of not knowing why for someone like me to start asking questions. According to [Cerebral Palsy Alliance](https://research.cerebralpalsy.org.au/what-is-cerebral-palsy/facts-about-cerebral-palsy/) (2013) 17 million people have CP worldwide, but before the age of 23, I had never met anyone with CP quite like me. I wondered why that was so I began searching for answers. I found many other young adults online who shared similar experiences or had yet to meet another with CP. I was shocked and saddened by this.

Over time, I grew frustrated with the reality that our larger, national disability organizations did not prioritize supporting young adults with CP in ways that foster connection and understanding. A leading disability organization here in the U.S. hosts an annual conference for its local affiliates but no longer opens its doors to public participation. Funds generally tend to go toward supporting early-intervention research and other costly forms of treatment for babies and young children. While medical treatment is crucial to improve quality of life (and gives hope to parents of young ones with CP) the fact is there is no cure for cerebral palsy. The rest of us go on living with it.

So why are our organizations not willing to openly address the emotional effects of having a lifelong disability? How should a seventeen year old in mainstream school explain their disability when asked, “What’s wrong with you?” Or, “What happened to you?” I still struggle with those questions. Must I always explain myself? Is it my job to make others more comfortable with the word disability? I wonder how or when to disclose my CP to potential employers as I graduate with my master’s degree. I could shy away from the conversation, or make up a lie (as recently depicted in Netflix’s _Special_) but I refuse, even when it might be easier to do so. I sense with great urgency that our community needs something more as these instances are not few and far between. And so the questions I had about advocacy and my place in it began to reveal answers:

*I stand for community, and I advocate for all those \#cpstrong.*

**[Read the rest on our Medium page](https://medium.com/medlab/building-spaces-for-belonging-7024f49ab071)**
