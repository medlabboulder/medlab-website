---
layout: event
title: "Alt Right vs. Anonymous: A Critical Comparison - with Gabriella Coleman"
summary: "Hear from a leading anthropologist of digital cultures"
tags: [events]
event-date: February 6, 2020
event-time: 5:00-6:30 p.m. Mountain Time
event-location: Hale Sciences 270
---

**[RSVP here](https://cloud.medlab.host/apps/forms/form/W2jfW1xNsdcMGS2w).**

<img alt="Anonymous logo, via Wikimedia Commons" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Anonymous_emblem.svg/480px-Anonymous_emblem.svg.png" style="width:40%; padding: 0 0 10 10;" align="right" />Today’s “Alt Right” or “Far Right” is a loose coalition of Internet trolls, White nationalists, men’s rights activists, game enthusiasts, and others. They have become a subject of intense scrutiny for their shrewd use of digital communications to recruit new participants, manipulate the news narrative, and nurture a political movement in support of the Trump presidency and its policies. Some critics compare this movement to one that emerged six years ago: Anonymous—the hacktivist collective that captured news headlines for its computer hacking sprees, vigilante justice operations, and support of social movements like Occupy and the Arab Spring. Coleman argues that these are two very different movements, notwithstanding that both emerge from habits of Internet trolling and transgression. She will compare Anonymous to the Alt Right, honing in on their origins, methods, tactics, and modes of recruitment, highlighting substantial differences and making the broader case for careful historical analysis in media studies work on virtual domains. The very classification of such disparate movements as “Internet activism” fails to capture the dynamics and importance of online tools for political movements today.

Gabriella (Biella) Coleman holds the Wolfe Chair in Scientific and Technological Literacy [at McGill University.](http://www.mcgill.ca/ahcs/people-contacts/faculty/gabriella-coleman) Trained as an anthropologist, her scholarship [covers the politics, cultures, and ethics of hacking](https://www.journals.uchicago.edu/doi/pdfplus/10.1086/688697). She is the author of two books on computer hackers and the founder and editor of [Hack_Curio](https://hackcur.io/), a video portal into the cultures of hacking. Her first book [Coding Freedom: The Ethics and Aesthetics of Hacking](http://www.amazon.com/Coding-Freedom-Ethics-Aesthetics-Hacking/dp/0691144613/ref=sr_1_1?ie=UTF8&qid=1419086140&sr=8-1&keywords=Coding+Freedom) was published in 2013 with Princeton University Press. She then published [Hacker, Hoaxer, Whistleblower, Spy: The Many Faces of Anonymous](http://www.versobooks.com/books/1749-hacker-hoaxer-whistleblower-spy) (Verso, 2014), which was named to [Kirkus Reviews Best Books of 2014](https://www.kirkusreviews.com/lists/best-current-affairs-books-2014/hacker-hoaxer-whistleblower-spy/) and was awarded the [Diana Forsythe Prize by the American Anthropological Association](http://blog.castac.org/2015/10/2015-forsythe/). Committed to public ethnography, she routinely presents her work to [diverse audiences](https://www.youtube.com/watch?v=A8mtG4oMzLs), teaches undergraduate and graduate courses, and has written for popular media outlets, including the New York Times, Slate, Wired, MIT Technology Review, Huffington Post, and the Atlantic. She sits on the board of [The Tor Project](https://www.torproject.org/).

[Hale Sciences 270](https://www.colorado.edu/map/?id=336#!m/193879)    
1350 Pleasant Street    
Boulder, CO 80302

*Presented by the Department of Media Studies and the Media Enterprise Design Lab.*

**[RSVP here](https://cloud.medlab.host/apps/forms/form/W2jfW1xNsdcMGS2w).**
