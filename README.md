# MEDLab website

**This website is now deprecated. The new MEDLab website is at [colorado.edu/lab/medlab](https://colorado.edu/lab/medlab).**

A website for the Media Enterprise Design Lab at the University of Colorado Boulder.
